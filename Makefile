## if root is installed, uncomment the comented lines to display histograms

CC = g++
INSTALL_PROGRAM = /usr/bin/install -c
exec_prefix = $(prefix)

PI_SOURCES = Random.C Primitive_Integrator.C PI_Demo.C
PI_HEADERS = Tools.H My_Limits.H MathTools.H Random.H Primitive_Integrator.H

EXTRA_DIST = COPYING

CXXFLAGS = -O2 -DUSING__PI_only -DUSING__ROOT -I`root-config --incdir`
ROOTFLAGS = -I`root-config --incdir` `root-config --glibs`

PI_DISTDIR = PI-1.0

all: PI_Demo

PI_Demo: $(addsuffix .o,$(basename $(PI_SOURCES)))

	$(CC) -o PI_Demo $(ROOTFLAGS) $(addsuffix .o,$(basename $(PI_SOURCES)))

clean: 
	-rm *.o PI_Demo

dist:
	mkdir $(PI_DISTDIR) && \
	cp $(PI_SOURCES) $(PI_HEADERS) \
	   $(EXTRA_DIST) Makefile $(PI_DISTDIR) && \
	tar -czf $(PI_DISTDIR).tar.gz $(PI_DISTDIR) && \
	rm -r $(PI_DISTDIR)

install:
	$(INSTALL_PROGRAM) -D PI_Demo $(exec_prefix)/bin/PI_Demo
